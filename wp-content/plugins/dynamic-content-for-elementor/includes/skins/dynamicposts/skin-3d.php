<?php
namespace DynamicContentForElementor\Includes\Skins;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Image_Size;

use DynamicContentForElementor\DCE_Helper;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Skin_3D extends Skin_Base {

	protected function _register_controls_actions() {
		parent::_register_controls_actions();

		add_action( 'elementor/element/dce-dynamicposts-v2/section_dynamicposts/after_section_end', [ $this, 'register_additional_3d_controls' ] );

	}

	public function get_script_depends() {
		return [];
    }
	public function get_style_depends() {
		return [];
    }

	public function get_id() {
		return '3d';
	}

	public function get_title() {
		return __( '3D', 'dynamic-content-for-elementor' );
	}

	public function register_additional_3d_controls() {

		$this->start_controls_section(
            'section_3d', [
	            'label' => '<i class="dynicon eicon-parallax"></i> '.__('3D', 'dynamic-content-for-elementor'),
	            'tab' => Controls_Manager::TAB_CONTENT,

            ]
        );
		$this->add_control(
            'type_3d', [
                'label' => __('Type of 3D', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SELECT,
                'default' => 'circle',
                'options' => [
                    'circle'     => 'Circle',
                    'fila'     => 'Row',

                ],
                'frontend_available' => true
            ]
        );
        $this->add_control(
            'size_plane_3d', [
                'label' => '<i class="fas fa-arrows-alt-h"></i>&nbsp;&nbsp;'.__('Size plane', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => '',
                ],
                'range' => [
                    'px' => [
                        'max' => 1200,
                        'min' => 0,
                        'step' => 1,
                    ]
                ],
                'render_type' => 'template',
                'frontend_available' => true,
                'selectors' => [
                    '{{WRAPPER}} .dce-posts-container.dce-skin-3d .dce-3d-element' => 'width: {{SIZE}}{{UNIT}};',
                ]
            ]
        );
        $this->add_control(
            'blur_depth_3d', [
                'label' => '<i class="fas fa-eye"></i>&nbsp;&nbsp;'.__('Depth blur', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SWITCHER,
                'default' => '',
                'frontend_available' => true,
                'condition' => [
                    $this->get_control_id('type_3d') => 'circle'
                ],
            ]
        );
        $this->add_control(
            'mousewheel_3d', [
                'label' => '<i class="fas fa-mouse"></i>&nbsp;&nbsp;'.__('Mouse wheel', 'dynamic-content-for-elementor'),
                'type' => Controls_Manager::SWITCHER,
                'default' => '',
                'frontend_available' => true
            ]
        );
        $this->end_controls_section();
	}

	protected function register_style_controls() {
		parent::register_style_controls();

		// $this->start_controls_section(
		// 	'section_style_3d',
		// 	[
		// 		'label' => __( '3D', 'dynamic-content-for-elementor' ),
		// 		'tab' => Controls_Manager::TAB_STYLE,
		// 	]
		// );

		// $this->end_controls_section();
	}

	protected function render_posts_before() {
		$_skin = $this->parent->get_settings('_skin');
		?>

		<div id="dce-scene-3d-container" class="dce-posts-wrapper"></div>
        <!-- <div class="dce-3d-trace"></div> -->
        <div class="dce-3d-navigation">
            <div class="dce-3d-prev dce-3d-arrow"><i class="fas fa-arrow-left"></i></div>
            <div class="dce-3d-next dce-3d-arrow"><i class="fas fa-arrow-right"></i></div>

        </div>
        <div class="dce-3d-quit"><i class="fas fa-times"></i></div>
<!--         <div id="menu">
            <button id="sphere">SPHERE</button>
            <button id="helix">HELIX</button>
            <button id="grid">GRID</button>
        </div> -->
		<?php
        /*
		?>
		<script>
			var dceAjaxPath = {"ajaxurl": "<?php echo admin_url('admin-ajax.php'); ?>"};
			var dce_listPosts_<?php echo $this->parent->get_id(); ?> = [
		<?php
		*/
	}

	/*protected function render_postsWrapper_after() {
		?>
		];
		</script>
		<?php
	}*/
	/*protected function render_post() {
		global $post;

		$separatorArray = '';
		if($this->counter) $separatorArray = ',';


		$p_id = $this->current_id;

		$p_title = get_the_title();

		$p_slug = $post->post_name;

		$p_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large');

		$p_author = get_the_author_meta('display_name');
		$p_authorimage = get_avatar_url(get_the_author_meta('ID'));

		$p_date = get_the_date($this->get_instance_value('3d_date_format'), '');

		$p_type = get_post_type_object(get_post_type())->rest_base;
		if(empty($p_type)) $p_type = get_post_type();

		$p_terms = '';
		$taxonomy = get_post_taxonomies($this->current_id);
		$cont = 0;
		$taxonomy_filter = $this->get_instance_value('3d_taxonomy_filter');

		foreach ($taxonomy as $tax) {
			if (isset($taxonomy_filter) && !empty($taxonomy_filter)) {
                if (!in_array($tax, $taxonomy_filter)) {
                    continue;
                }
            }
			if($tax != 'post_format'){
				$term_list = DCE_Helper::get_the_terms_ordered($this->current_id, $tax);
				if ($term_list && is_array($term_list) && count($term_list) > 0) {

					foreach ($term_list as $key => $term) {
						$sep = $cont ? ',' : '';
						$p_terms .= $sep.$term->name;
						$cont ++;
					}
				}
			}
		}
		$p_link = $this->current_permalink;


		echo $separatorArray.'{"id":"' . $p_id . '",';
		echo '"index":"' . $this->counter . '",';
		echo '"slug":"' . $p_slug . '",';
		echo '"title":"' . $p_title . '",';
		echo '"link":"' . $p_link . '",';
        echo '"image":"' . $p_image[0] . '",';
        echo '"author":"' . $p_author . '",';
        echo '"authorimage":"' . $p_authorimage . '",';
        echo '"date":"' . $p_date . '",';
        echo '"type":"' . $p_type . '",';
        echo '"terms": "' . $p_terms . '"}';


		$this->counter ++;
	}*/

	// Classes ----------
	public function get_container_class() {
		return 'dce-3d-container dce-skin-' . $this->get_id();
	}
    public function get_wrapper_class() {
        return 'dce-grid-3d dce-3d-wrapper dce-3d-wrapper-hidden dce-wrapper-' . $this->get_id();
    }
    public function get_item_class() {
        return 'dce-item-' . $this->get_id();
    }

    public function get_image_class() {
        return '';
    }
}
