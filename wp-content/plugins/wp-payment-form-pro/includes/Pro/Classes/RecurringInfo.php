<?php

namespace WPPayForm\Pro\Classes;

use WPPayForm\Classes\ArrayHelper;
use WPPayForm\Classes\GeneralSettings;
use WPPayForm\Classes\Models\Forms;
use WPPayForm\Classes\Models\Submission;
use WPPayForm\Classes\Models\Subscription;
use WPPayForm\Classes\Models\SubscriptionTransaction;

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Recutting Payment View Actions
 * @since 1.0.0
 */
class RecurringInfo
{
    public function register()
    {
        add_filter('wppayform/form_entry', array($this, 'addRecurringSubscriptions'), 10, 1);
        add_action('wppayform/after_delete_submission', array($this, 'deleteSubscriptionData'), 99, 1);
    }

    public function addRecurringSubscriptions($submission)
    {
        $subscriptionModel = new Subscription();
        $subscriptionTransactionModel = new SubscriptionTransaction();
        $subscriptions = $subscriptionModel->getSubscriptions($submission->id);

        $subscriptionPaymentTotal = 0;

        foreach ($subscriptions as $subscription) {
            $related_payments = $subscriptionTransactionModel->getSubscriptionTransactions($subscription->id);
            foreach ($related_payments as $related_payment) {
                if ($related_payment->status == 'paid') {
                    $subscriptionPaymentTotal += $related_payment->payment_total;
                }
                $related_payment->view_url = $this->getTransactionUrl($related_payment);
            }
            $subscription->related_payments = $related_payments;
        }

        $submission->subscription_payment_total = $subscriptionPaymentTotal;
        $submission->subscriptions = $subscriptions;
        return $submission;
    }

    public function deleteSubscriptionData($submissionId)
    {
        $allSubscriptions = wpFluent()->table('wpf_subscriptions')
            ->where('submission_id', $submissionId)
            ->get();
        if (!$allSubscriptions) {
            return;
        }
        $subscriptionIds = [];
        foreach ($allSubscriptions as $subscription) {
            $subscriptionIds[] = $subscription->id;
        }

        wpFluent()->table('wpf_order_transactions')
            ->where('transaction_type', 'subscription')
            ->whereIn('id', $subscriptionIds)
            ->delete();

        wpFluent()->table('wpf_subscriptions')
            ->where('submission_id', $submissionId)
            ->delete();
    }

    private function getTransactionUrl($transaction)
    {
        if ($transaction->payment_mode == 'test') {
            return 'https://dashboard.stripe.com/test/payments/' . $transaction->charge_id;
        }
        return 'https://dashboard.stripe.com/payments/' . $transaction->charge_id;
    }
}