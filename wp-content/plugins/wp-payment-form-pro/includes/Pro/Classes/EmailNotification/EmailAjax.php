<?php

namespace WPPayForm\Pro\Classes\EmailNotification;


use WPPayForm\Classes\AccessControl;
use WPPayForm\Classes\FormPlaceholders;

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Ajax Handler Class for Email Notification
 * @since 1.0.0
 */
class EmailAjax
{

    public function register()
    {
        add_action('wppayform/admin_ajax_handler_catch', array($this, 'handle'));
    }

    public function handle($route)
    {
        $validRoutes = array(
            'get_email_notifications'  => 'getNotifications',
            'save_email_notifications' => 'saveNotifications'
        );

        if (isset($validRoutes[$route])) {
            AccessControl::checkAndPresponseError($route, 'forms');
            do_action('wppayform/doing_ajax_forms_' . $route);
            return $this->{$validRoutes[$route]}();
        }
    }

    public function getNotifications()
    {
        $formId = intval($_REQUEST['form_id']);
        $notifications = get_post_meta($formId, 'wpf_email_notifications', true);
        if (!$notifications) {
            $notifications = array();
        }

        $notificationActions = array(
            'wppayform/after_form_submission_complete' => array(
                'hook_name'   => 'wppayform/after_form_submission_complete',
                'hook_title'  => 'After Form Submission',
                'description' => 'Send email when the form will be submitted. Please note that, If you select this, Email will be sent even form payment (if any) failed'
            ),
            'wppayform/form_payment_success' => array(
                'hook_name'   => 'wppayform/form_payment_success',
                'hook_title'  => 'On Payment Success',
                'description' => 'This email will be sent after payment successfully made (if you have payment enabled)'
            )
        );

        $notificationActions = apply_filters('wppayform/email_notification_actions', $notificationActions, $formId);

        wp_send_json_success(array(
            'notifications'        => $notifications,
            'merge_tags'           => FormPlaceholders::getAllPlaceholders($formId),
            'notification_actions' => array_values($notificationActions)
        ), 200);
    }

    public function saveNotifications()
    {
        $formId = intval($_REQUEST['form_id']);
        $notifications = wp_unslash($_REQUEST['notifications']);
        update_post_meta($formId, 'wpf_email_notifications', $notifications);

        wp_send_json_success(array(
            'message' => __('Email Notifications has been updated', 'wppayform')
        ), 200);
    }
}